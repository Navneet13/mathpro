package com.navneet.solvemath;

import android.os.AsyncTask;

import com.navneet.solvemath.pojo.XMLParser;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FetchStepsTask extends AsyncTask<FetchStepsTask.UploadParams, Void, FetchStepsTask.Result> {

    private final ResultListener listener;

    FetchStepsTask(ResultListener listener) {
        this.listener = listener;
    }

    @Override
    protected Result doInBackground(UploadParams... arr) {
        UploadParams params = arr[0];
        Result result;
        try {
            OkHttpClient client = new OkHttpClient();
            String urlString = Constant.fetchStepsURL + params.expression;

            Request request = new Request.Builder()
                    .url(urlString)
                    .addHeader("content-type", "application/json")
                    .build();
            Response response = client.newCall(request).execute();
            String responseString = response.body().string();

            //use ByteArrayInputStream to get the bytes of the String and convert them to InputStream.
//            String res = "<?xml version='1.0' encoding='UTF-8'?>\n" +
//                    "<queryresult success='true'\n" +
//                    "    error='false'\n" +
//                    "    numpods='4'\n" +
//                    "    datatypes='Solve'\n" +
//                    "    timedout=''\n" +
//                    "    timedoutpods=''\n" +
//                    "    timing='1.192'\n" +
//                    "    parsetiming='0.446'\n" +
//                    "    parsetimedout='false'\n" +
//                    "    recalculate=''\n" +
//                    "    id='MSP672310fa6e413d0f265c00000i9h0hi06173aa1g'\n" +
//                    "    host='https://www5a.wolframalpha.com'\n" +
//                    "    server='56'\n" +
//                    "    related='https://www5a.wolframalpha.com/api/v1/relatedQueries.jsp?id=MSPa672410fa6e413d0f265c000045b53daa5f0defhd1335321721000003642'\n" +
//                    "    version='2.6'>\n" +
//                    " <pod title='Input interpretation'\n" +
//                    "     scanner='Identity'\n" +
//                    "     id='Input'\n" +
//                    "     position='100'\n" +
//                    "     error='false'\n" +
//                    "     numsubpods='1'>\n" +
//                    "  <subpod title=''>\n" +
//                    "   <plaintext>solve 3 x - 7 = 10</plaintext>\n" +
//                    "  </subpod>\n" +
//                    "  <expressiontypes count='1'>\n" +
//                    "   <expressiontype name='Default' />\n" +
//                    "  </expressiontypes>\n" +
//                    " </pod>\n" +
//                    " <pod title='Results'\n" +
//                    "     scanner='Solve'\n" +
//                    "     id='Result'\n" +
//                    "     position='200'\n" +
//                    "     error='false'\n" +
//                    "     numsubpods='2'\n" +
//                    "     primary='true'>\n" +
//                    "  <subpod title=''>\n" +
//                    "   <plaintext>x = 17/3</plaintext>\n" +
//                    "  </subpod>\n" +
//                    "  <subpod title='Possible intermediate steps'>\n" +
//                    "   <plaintext>Solve for x:\n" +
//                    "3 x - 7 = 10\n" +
//                    "Add 7 to both sides:\n" +
//                    "3 x + (7 - 7) = 7 + 10\n" +
//                    "7 - 7 = 0:\n" +
//                    "3 x = 10 + 7\n" +
//                    "10 + 7 = 17:\n" +
//                    "3 x = 17\n" +
//                    "Divide both sides of 3 x = 17 by 3:\n" +
//                    "(3 x)/3 = 17/3\n" +
//                    "3/3 = 1:\n" +
//                    "Answer: | \n" +
//                    " | x = 17/3</plaintext>\n" +
//                    "  </subpod>\n" +
//                    "  <expressiontypes count='2'>\n" +
//                    "   <expressiontype name='Default' />\n" +
//                    "   <expressiontype name='Default' />\n" +
//                    "  </expressiontypes>\n" +
//                    "  <states count='2'>\n" +
//                    "   <state name='Approximate form'\n" +
//                    "       input='Result__Approximate form' />\n" +
//                    "   <state name='Hide steps'\n" +
//                    "       input='Result__Hide steps' />\n" +
//                    "  </states>\n" +
//                    " </pod>\n" +
//                    " <pod title='Plot'\n" +
//                    "     scanner='Solve'\n" +
//                    "     id='RootPlot'\n" +
//                    "     position='300'\n" +
//                    "     error='false'\n" +
//                    "     numsubpods='1'>\n" +
//                    "  <subpod title=''>\n" +
//                    "   <plaintext></plaintext>\n" +
//                    "  </subpod>\n" +
//                    "  <expressiontypes count='1'>\n" +
//                    "   <expressiontype name='Default' />\n" +
//                    "  </expressiontypes>\n" +
//                    " </pod>\n" +
//                    " <pod title='Number line'\n" +
//                    "     scanner='Solve'\n" +
//                    "     id='NumberLine'\n" +
//                    "     position='400'\n" +
//                    "     error='false'\n" +
//                    "     numsubpods='1'>\n" +
//                    "  <subpod title=''>\n" +
//                    "   <plaintext></plaintext>\n" +
//                    "  </subpod>\n" +
//                    "  <expressiontypes count='1'>\n" +
//                    "   <expressiontype name='1DMathPlot' />\n" +
//                    "  </expressiontypes>\n" +
//                    " </pod>\n" +
//                    "</queryresult>";

            result = new ResultSuccessful(new XMLParser().parse(responseString));
        } catch (Exception e) {
            result = new ResultFailed(e.getLocalizedMessage());
        }
        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        if (result instanceof ResultSuccessful) {
            ResultSuccessful successful = (ResultSuccessful) result;
            listener.onSuccess(successful.stepDataList);
        } else if (result instanceof ResultFailed) {
            ResultFailed failed = (ResultFailed) result;
            listener.onError(failed.message);
        }
    }

    interface ResultListener {
        void onError(String message);

        void onSuccess(List<String> stepDataList);
    }

    static class UploadParams {
        private String expression;

        UploadParams(String expression) {
            this.expression = expression;
        }
    }

    static class Result {
    }

    private static class ResultSuccessful extends Result {
        List<String> stepDataList;

        ResultSuccessful(List<String> stepDataList) {
            this.stepDataList = stepDataList;
        }
    }

    private static class ResultFailed extends Result {
        String message;

        ResultFailed(String message) {
            this.message = message;
        }
    }
}