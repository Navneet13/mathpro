package com.navneet.solvemath;

public interface Constant {
    public static String app_id = "rippy_multani_gmail_com";
    public static String app_key = "e3673a7be9b0581eff04";
    public static String fetchImageURL = "https://api.mathpix.com/v3/latex";

    public static String fetchStepsURL = "http://api.wolframalpha.com/v2/query?&podstate=Result__Step-by-step+solution&format=plaintext&appid=RK799H-R7WW5XE2RQ&input=solve ";
}
