package com.navneet.solvemath;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowAllStepsActivity extends AppCompatActivity {

    @BindView(R.id.showStepsTextView)
    TextView stepsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all_steps);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        if (bundle != null) {
            ArrayList<String> steps = bundle.getStringArrayList("data");
            if (steps != null) {
                stepsTextView.setText(createString(steps));
            }
        }
    }

    private String createString(ArrayList<String> stepsList) {
        String fullString = "";
        for (String s : stepsList) {
            fullString = new StringBuilder().append(fullString).append(s).append("\n\n").toString();
        }
        return fullString;
    }
}
