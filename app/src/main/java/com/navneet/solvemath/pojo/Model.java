package com.navneet.solvemath.pojo;

import java.util.List;

public class Model {

    List<String> fetchedData;

    public Model(List<String> fetchedData) {
        this.fetchedData = fetchedData;
    }

    public List<String> getFetchedData() {
        return fetchedData;
    }

    public void setFetchedData(List<String> fetchedData) {
        this.fetchedData = fetchedData;
    }
}
