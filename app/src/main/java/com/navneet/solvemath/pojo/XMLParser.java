package com.navneet.solvemath.pojo;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLParser {

    // We don't use namespaces
    private static final String ns = null;

    public List<String> parse(String responseString) throws ParserConfigurationException, IOException, SAXException {

        //document contains the complete XML as a Tree.
        Document document = getDomElement(responseString);

        List<String> stepsList = new ArrayList<>();

        //Iterating through the nodes and extracting the data.
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            //We have encountered an <pod> tag.
            Node node = nodeList.item(i);
            if (node instanceof Element && "pod".equals(node.getNodeName())) {
                Pod pod = new Pod();
                NodeList podNodeList = node.getChildNodes();
                for (int j = 0; j < podNodeList.getLength(); j++) {
                    Node podNode = podNodeList.item(j);
                    if (podNode instanceof Element && "subpod".equals(podNode.getNodeName())) {
                        NodeList subPodList = podNode.getChildNodes();
                        for (int k = 0; k < subPodList.getLength(); k++) {
                            Node subPodNode = subPodList.item(k);
                            if (subPodNode instanceof Element && "plaintext".equals(subPodNode.getNodeName())) {
                                String plainText = subPodNode.getTextContent();
                                if (!plainText.isEmpty()) {
                                    stepsList.add(plainText);
                                }
                            }
                        }
                    }
                }
            }
        }
        return stepsList;
    }

    /**
     * Getting XML DOM element
     *
     * @param xml string
     */
    public Document getDomElement(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }

        return doc;
    }

//    public List parse(InputStream in) throws XmlPullParserException, IOException {
//        try {
//            XmlPullParser parser = Xml.newPullParser();
//            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
//            parser.setInput(in, null);
//            parser.nextTag();
//            return readFeed(parser);
//        } finally {
//            in.close();
//        }
//    }
//
//    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
//        List entries = new ArrayList();
//
//        parser.require(XmlPullParser.START_TAG, ns, "queryresult");
//        while (parser.next() != XmlPullParser.END_TAG) {
//            if (parser.getEventType() != XmlPullParser.START_TAG) {
//                continue;
//            }
//            String name = parser.getName();
//            // Starts by looking for the entry tag
//            if (name.equals("pod")) {
//                entries.add(readPodEntry(parser));
//            }
//        }
//        return entries;
//    }
//
//    private Pod readPodEntry(XmlPullParser parser) throws IOException, XmlPullParserException {
//
//        Pod pod = new Pod();
//        parser.require(XmlPullParser.START_TAG, ns, "pod");
//        while (parser.next() != XmlPullParser.END_TAG) {
//            if (parser.getEventType() != XmlPullParser.START_TAG) {
//                continue;
//            }
//            String name = parser.getName();
//            // Starts by looking for the entry tag
//            if (name.equals("subpod")) {
////                pod.setSubpod(readSubPodEntry(parser));
//            }
//
//            if (name.equals("plaintext")) {
//
//            }
//        }
//        return pod;
//    }
//
//    private Subpod readSubPodEntry(XmlPullParser parser) throws IOException, XmlPullParserException {
//
//        Subpod subPod = new Subpod();
//        parser.require(XmlPullParser.START_TAG, ns, "subpod");
//        while (parser.next() != XmlPullParser.END_TAG) {
//            if (parser.getEventType() != XmlPullParser.START_TAG) {
//                continue;
//            }
//            String name = parser.getName();
//            // Starts by looking for the entry tag
//            if (name.equals("plaintext")) {
//                if (parser.next() == XmlPullParser.TEXT) {
//                    subPod.setPlaintext(parser.getText());
//                    parser.nextTag();
//                }
//            }
//        }
//        return subPod;
//    }
//
//    private void parseData(XmlPullParser parser) throws XmlPullParserException {
//
//        int event = parser.getEventType();
//        while (event != XmlPullParser.END_DOCUMENT) {
//            String name = parser.getName();
//            switch (event) {
//                case XmlPullParser.START_TAG:
//                    break;
//
//                case XmlPullParser.END_TAG:
//
//                    break;
//            }
////            event = parser.next();
//        }
//    }
}