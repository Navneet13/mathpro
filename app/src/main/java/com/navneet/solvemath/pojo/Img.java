package com.navneet.solvemath.pojo;

public class Img {
    private String _src;
    private String _alt;
    private String _title;
    private String _width;
    private String _height;
    private String _type;
    private String _themes;
    private String _colorinvertable;


    // Getter Methods

    public String get_src() {
        return _src;
    }

    public String get_alt() {
        return _alt;
    }

    public String get_title() {
        return _title;
    }

    public String get_width() {
        return _width;
    }

    public String get_height() {
        return _height;
    }

    public String get_type() {
        return _type;
    }

    public String get_themes() {
        return _themes;
    }

    public String get_colorinvertable() {
        return _colorinvertable;
    }

    // Setter Methods

    public void set_src(String _src) {
        this._src = _src;
    }

    public void set_alt(String _alt) {
        this._alt = _alt;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public void set_width(String _width) {
        this._width = _width;
    }

    public void set_height(String _height) {
        this._height = _height;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public void set_themes(String _themes) {
        this._themes = _themes;
    }

    public void set_colorinvertable(String _colorinvertable) {
        this._colorinvertable = _colorinvertable;
    }
}

