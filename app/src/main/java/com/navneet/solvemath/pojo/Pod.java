package com.navneet.solvemath.pojo;

public class Pod {
    Subpod subpod;
    ExpressionType ExpressiontypesObject;
    private String _title;
    private String _scanner;
    private String _id;
    private String _position;
    private String _error;
    private String _numsubpods;


    // Getter Methods

    public Subpod getSubpod() {
        return subpod;
    }

    public ExpressionType getExpressiontypes() {
        return ExpressiontypesObject;
    }

    public String get_title() {
        return _title;
    }

    public String get_scanner() {
        return _scanner;
    }

    public String get_id() {
        return _id;
    }

    public String get_position() {
        return _position;
    }

    public String get_error() {
        return _error;
    }

    public String get_numsubpods() {
        return _numsubpods;
    }

    // Setter Methods

    public void setSubpod(Subpod subpodObject) {
        this.subpod = subpodObject;
    }

    public void setExpressiontypes(ExpressionType expressiontypesObject) {
        this.ExpressiontypesObject = expressiontypesObject;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public void set_scanner(String _scanner) {
        this._scanner = _scanner;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void set_position(String _position) {
        this._position = _position;
    }

    public void set_error(String _error) {
        this._error = _error;
    }

    public void set_numsubpods(String _numsubpods) {
        this._numsubpods = _numsubpods;
    }
}

