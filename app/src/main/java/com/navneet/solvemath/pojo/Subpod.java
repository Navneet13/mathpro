package com.navneet.solvemath.pojo;

public class Subpod {
    private String plaintext;
    private String _title;


    // Getter Methods

    public String getPlaintext() {
        return plaintext;
    }

    public String get_title() {
        return _title;
    }

    // Setter Methods

    public void setPlaintext(String plaintext) {
        this.plaintext = plaintext;
    }

    public void set_title(String _title) {
        this._title = _title;
    }
}