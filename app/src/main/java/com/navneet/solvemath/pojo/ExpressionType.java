package com.navneet.solvemath.pojo;

public class ExpressionType {

        ExpressionType expressionTypeObject;
        private String _count;


        // Getter Methods

        ExpressionType getExpressiontype() {
            return expressionTypeObject;
        }

        public String get_count() {
            return _count;
        }

        // Setter Methods

        public void setExpressiontype(ExpressionType expressionTypeObject) {
            this.expressionTypeObject = expressionTypeObject;
        }

        public void set_count(String _count) {
            this._count = _count;
        }
    }
