package com.navneet.solvemath.pojo;

public class StepData {

    Queryresult queryResultObject;

    // Getter Methods
    public Queryresult getQueryresult() {
        return queryResultObject;
    }

    // Setter Methods
    public void setQueryresult(Queryresult queryResultObject) {
        this.queryResultObject = queryResultObject;
    }
}