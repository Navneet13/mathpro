package com.navneet.solvemath.pojo;

import java.util.ArrayList;

public class Queryresult {
    ArrayList<Pod> pod = new ArrayList();
    private String _success;
    private String _error;
    private String _numpods;
    private String _datatypes;
    private String _timedout;
    private String _timedoutpods;
    private String _timing;
    private String _parsetiming;
    private String _parsetimedout;
    private String _recalculate;
    private String _id;
    private String _host;
    private String _server;
    private String _related;
    private String _version;

    // Getter Methods

    public String get_success() {
        return _success;
    }

    public String get_error() {
        return _error;
    }

    public String get_numpods() {
        return _numpods;
    }

    public String get_datatypes() {
        return _datatypes;
    }

    public String get_timedout() {
        return _timedout;
    }

    public String get_timedoutpods() {
        return _timedoutpods;
    }

    public String get_timing() {
        return _timing;
    }

    public String get_parsetiming() {
        return _parsetiming;
    }

    public String get_parsetimedout() {
        return _parsetimedout;
    }

    public String get_recalculate() {
        return _recalculate;
    }

    public String get_id() {
        return _id;
    }

    public String get_host() {
        return _host;
    }

    public String get_server() {
        return _server;
    }

    public String get_related() {
        return _related;
    }

    public String get_version() {
        return _version;
    }

    // Setter Methods

    public void set_success(String _success) {
        this._success = _success;
    }

    public void set_error(String _error) {
        this._error = _error;
    }

    public void set_numpods(String _numpods) {
        this._numpods = _numpods;
    }

    public void set_datatypes(String _datatypes) {
        this._datatypes = _datatypes;
    }

    public void set_timedout(String _timedout) {
        this._timedout = _timedout;
    }

    public void set_timedoutpods(String _timedoutpods) {
        this._timedoutpods = _timedoutpods;
    }

    public void set_timing(String _timing) {
        this._timing = _timing;
    }

    public void set_parsetiming(String _parsetiming) {
        this._parsetiming = _parsetiming;
    }

    public void set_parsetimedout(String _parsetimedout) {
        this._parsetimedout = _parsetimedout;
    }

    public void set_recalculate(String _recalculate) {
        this._recalculate = _recalculate;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void set_host(String _host) {
        this._host = _host;
    }

    public void set_server(String _server) {
        this._server = _server;
    }

    public void set_related(String _related) {
        this._related = _related;
    }

    public void set_version(String _version) {
        this._version = _version;
    }
}