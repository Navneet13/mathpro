package com.navneet.solvemath;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Login extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    @BindView(R.id.login)
    Button login;

    @BindView(R.id.btnCreateAccount)
    Button btnCreateAccount;

    @BindView(R.id.mail)
    TextView mail;

    @BindView(R.id.pwd)
    TextView pwd;

    private FirebaseAuth mAuth;
    private GoogleSignInClient googleSignInClient;
    private ProgressDialog progressDialog;
    private static final String TAG = "Solve_Math";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        //getting firebase auth object
        mAuth = FirebaseAuth.getInstance();
        //If the objects getcurrentuser() is not null means user is already logged in

        mail.setOnFocusChangeListener(this);
        pwd.setOnFocusChangeListener(this);
        progressDialog = new ProgressDialog(this);
        login.setOnClickListener(this);
        btnCreateAccount.setOnClickListener(this);


        if (mAuth.getCurrentUser() != null) {
            //close this activity
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    boolean notEmpty() {
        if (mail.getText().length() != 0)
            if (pwd.getText().length() != 0)
                return true;
            else {
                Toast.makeText(getApplicationContext(), "Password not entered.", Toast.LENGTH_SHORT).show();
                return false;
            }
        else {
            Toast.makeText(getApplicationContext(), "Email ID not entered", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void userLogin() {
        String email = mail.getText().toString().trim();
        String psswrd = pwd.getText().toString().trim();
        if (notEmpty()) {
            progressDialog.setMessage("Signing In please wait.....");
            progressDialog.show();
            mAuth.signInWithEmailAndPassword(email, psswrd)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            //if the task is successful
                            if (task.isSuccessful()) {
                                //start the profile activity
                                finish();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), "Something went wrong.Aren't you registered??", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            if (v == mail) {
                String email = mail.getText().toString();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mail.setError("Please enter a valid email address");
                } else {
                    Log.i("Yay!", "Email is valid.");
                    mail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.okicon, 0);
                }
            } else if (v == pwd) {
                if (pwd.getText().toString().length() < 6) {
                    pwd.setError("Password should be greater than 6 characters!");
                } else {
                    Drawable myIcon = getResources().getDrawable(R.drawable.okicon);
                    myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
                    pwd.setError("Good", myIcon);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == login) {
            userLogin();
        } else if (v == btnCreateAccount) {
            startActivity(new Intent(getApplicationContext(), SignUp.class));
        }

    }


}

