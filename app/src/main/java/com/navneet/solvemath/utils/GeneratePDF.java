package com.navneet.solvemath.utils;

import android.content.ActivityNotFoundException;
import android.os.Environment;
import android.os.FileUtils;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.navneet.solvemath.MyApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class GeneratePDF {

    private String equation;
    private List<String> stepsList;

    public GeneratePDF(String equation, List<String> stepsList) {
        this.equation = equation;
        this.stepsList = stepsList;
    }

    /***
     * Variables for further use....
     */
    BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
    float mHeadingFontSize = 20.0f;
    float mValueFontSize = 26.0f;

    // LINE SEPARATOR
    LineSeparator lineSeparator = new LineSeparator();

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void generatePDF(String destination) throws DocumentException, IOException {

        createPdf(destination);
//        if (new File(destination).exists()) {
//            new File(destination).delete();
//        }
//
//        /*
//         * Creating Document
//         */
//        Document document = new Document();
//
//        // Destination
//        File dir = new File(destination);
//        if(!dir.exists())
//            dir.mkdirs();
//
//        File pdfFile = new File(dir.getAbsolutePath() + equation+".txt");
//        if (!pdfFile.exists()) {
//            if (pdfFile.createNewFile())
//                System.out.println("File created");
//            else
//                System.out.println("File already exists");
//        }
//
//
//
//        FileOutputStream fOut = new FileOutputStream(pdfFile);
//
//        // Location to save
//        PdfWriter.getInstance(document, fOut);
//
//        // Open to write
//        document.open();
//
//        // Document Settings
//        document.setPageSize(PageSize.A4);
//        document.addCreationDate();
////        document.addAuthor("Solve Math");
//        document.addCreator("Solve Math");
//
//        lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));
//
//        addEquationTitle(document);
//
//        for (int i = 0; i < stepsList.size(); i++) {
//            addStep(document, stepsList.get(i));
//        }
//        document.close();
    }

    private void addEquationTitle(Document document) throws IOException, DocumentException {

        BaseFont urName = BaseFont.createFont("Helvetica", "UTF-8", BaseFont.EMBEDDED);
        // Adding Title....
        Font equationTitleFont = new Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK);
        // Creating Chunk
        Chunk stepsDetail = new Chunk("Steps", equationTitleFont);

        // Creating Paragraph to add...
        Paragraph equationParagraph = new Paragraph(equation);
        // Setting Alignment for Heading
        equationParagraph.setAlignment(Element.ALIGN_CENTER);
        // Finally Adding that Chunk
        document.add(equationParagraph);
    }

    private void addStep(Document document, String step) throws IOException, DocumentException {

        BaseFont urName = BaseFont.createFont("Helvetica", "UTF-8", BaseFont.EMBEDDED);
        // Adding Title....
        Font equationTitleFont = new Font(urName, 18.0f, Font.NORMAL, BaseColor.BLACK);
        // Creating Chunk
        Chunk stepsDetail = new Chunk("Steps", equationTitleFont);
        // Creating Paragraph to add...
        Paragraph equationParagraph = new Paragraph(step);
        // Setting Alignment for Heading
        equationParagraph.setAlignment(Element.ALIGN_CENTER);
        // Finally Adding that Chunk
        document.add(equationParagraph);
        document.add(lineSeparator);
    }

    private void createPdf(String dest) {

        try {

            File file = new File(dest);

            if (!file.exists()) {
                file.mkdirs();
            }

            File pdfFile = new File(file.getAbsolutePath() + "dest.pdf");
            if (!pdfFile.exists()) {
                if (pdfFile.createNewFile()) {
                    System.out.println("Created");
                }
            }

            // Creating Document
            Document document = new Document();

            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

            // Open to write
            document.open();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("Navneet Kaur");
            document.addCreator("Solve Math");

            BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));
            Font dateFont = new Font(urName, mValueFontSize, Font.NORMAL, mColorAccent);
            Chunk stepChunk;
            for (String step : stepsList) {
                // Step
                stepChunk = new Chunk(step, dateFont);
                Paragraph stepParagraph = new Paragraph(stepChunk);
                stepParagraph.setAlignment(Element.ALIGN_LEFT);
                document.add(stepParagraph);

                document.add(new Paragraph(""));
                document.add(new Chunk(lineSeparator));
                document.add(new Paragraph(""));
            }

            if (stepsList != null && stepsList.size() > 0) {
                document.close();
//                FileUtils.openFile(this, new File(dest));
            }

        } catch (IOException | DocumentException ie) {
            Log.e("TAG", "createPdf: Error " + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
//            Toast.makeText(getContext(), "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }
}
