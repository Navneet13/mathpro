package com.navneet.solvemath.utils;

import android.content.Context;

import com.navneet.solvemath.R;

import java.io.File;

public class FileUtils {

    /**
     * Get Path of App which contains Files
     *
     * @return path of root dir
     */
    public static String getAppPath(Context context) {
        File dir = new File(android.os.Environment.getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name)
                + File.separator);
        if (!dir.exists()) {
            if (dir.mkdirs()) {
                System.out.println("Created");
            }
        }
        return dir.getPath() + File.separator;
    }
}
