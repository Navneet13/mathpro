package com.navneet.solvemath;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.StepViewHolder> {

    private List<String> equationList;
    private Context context;
    private HashMap<String, List<String>> map;

    StepAdapter(List<String> equationList, Context context, HashMap<String, List<String>> map) {
        this.equationList = equationList;
        this.context = context;
        this.map = map;
    }

    @NonNull
    @Override
    public StepViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_steps_layout, parent, false);
        return new StepViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StepViewHolder holder, int position) {
        holder.equationTextView.setText(equationList.get(position));
    }

    @Override
    public int getItemCount() {
        if (equationList != null) {
            return equationList.size();
        }
        return 0;
    }

    class StepViewHolder extends RecyclerView.ViewHolder {
        private TextView equationTextView;

        StepViewHolder(@NonNull View itemView) {
            super(itemView);
            equationTextView = itemView.findViewById(R.id.equationTextView);
            equationTextView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ShowAllStepsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                Bundle bundle = new Bundle();
                bundle.putStringArrayList("data", (ArrayList<String>) map.get(equationList.get(getAdapterPosition())));

                intent.putExtra("bundle", bundle);
                context.startActivity(intent);
            });
        }
    }
}
