package com.navneet.solvemath;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.mail)
    EditText mail;

    @BindView(R.id.pwd)
    EditText pwd;

    @BindView(R.id.repwd)
    EditText repwd;

    @BindView(R.id.register)
    Button register;

    @BindView(R.id.clear)
    Button clear;

    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;

    boolean notEmpty() {

        if (mail.getText().length() != 0)
            if (pwd.getText().length() != 0)
                if (repwd.getText().length() != 0)
                    return true;
                else {
                    Toast.makeText(getApplicationContext(), "Re-enter password.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            else {
                Toast.makeText(getApplicationContext(), "Password not entered.", Toast.LENGTH_SHORT).show();
                return false;
            }
        else {
            Toast.makeText(getApplicationContext(), "Email ID not entered", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    boolean matchPwd() {
        if (pwd.getText().toString().equals(repwd.getText().toString()))
            return true;
        else
            Toast.makeText(getApplicationContext(), "Passwords mismatch.", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) { //to check whether user is logged in or not
            finish();

            //open next activity u want
            startActivity(new Intent(getApplicationContext(), MainActivity.class));

        }

        progressDialog = new ProgressDialog(this);
        register.setOnClickListener(this);
        clear.setOnClickListener(this);

        mail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String email = mail.getText().toString();
                    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        mail.setError("Please enter a valid email address");
                    } else {
                        mail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.okicon, 0);
                    }
                }
            }
        });

        pwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (pwd.getText().toString().length() < 6) {
                        pwd.setError("Password should be greater than 6 characters");
                    } else {
                        Drawable myIcon = getResources().getDrawable(R.drawable.okicon);
                        myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
                        pwd.setError("Good", myIcon);
                    }
                }
            }
        });

    }

    private void clear() {
        mail.setText(null);
        pwd.setText(null);
        repwd.setText(null);
    }

    private void registeruser() {
        String Mail = mail.getText().toString();
        String Password = pwd.getText().toString();
        if (notEmpty() && matchPwd()) {
            progressDialog.setMessage("Registering..Please Wait");
            progressDialog.show();
        }

        mAuth.createUserWithEmailAndPassword(Mail, Password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    finish();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                } else
                    Toast.makeText(getApplicationContext(), "Registration failed", Toast.LENGTH_LONG).show();
            }

        });
        progressDialog.dismiss();
    }


    @Override
    public void onClick(View v) {
        if (v == register) {
            registeruser();
        }
        if (v == clear) {
            clear();
        }
    }
}
