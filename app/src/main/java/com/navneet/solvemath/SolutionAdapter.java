package com.navneet.solvemath;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SolutionAdapter extends RecyclerView.Adapter<SolutionAdapter.SolutionViewHolder> {

    private List<String> stepList;

    SolutionAdapter(List<String> stepList) {
        this.stepList = stepList;
    }

    @NonNull
    @Override
    public SolutionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recycler_view, parent, false);
        return new SolutionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SolutionViewHolder holder, int position) {
        holder.textView.setText(stepList.get(position));
    }

    @Override
    public int getItemCount() {
        if (stepList != null) {
            return stepList.size();
        }
        return 0;
    }

    class SolutionViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        SolutionViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.item);
        }
    }
}
