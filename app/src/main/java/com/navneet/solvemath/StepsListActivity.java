package com.navneet.solvemath;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepsListActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @BindView(R.id.stepsRecyclerView)
    RecyclerView stepsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps_list);

        ButterKnife.bind(this);

        fetchList();
    }

    private void fetchList() {

        List<String> equations = new ArrayList<>();
        HashMap<String, List<String>> equationsStepMap = new HashMap<>();

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            String uID = mAuth.getUid();
            if (uID != null) {
                DatabaseReference databaseReference = database.getReference(uID);
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot imageSnapshot: dataSnapshot.getChildren()) {
                            equations.add(imageSnapshot.getKey());
                            equationsStepMap.put(imageSnapshot.getKey(), (List<String>) imageSnapshot.getValue());
                            stepsRecyclerView.getAdapter().notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        }

        setUpRecyclerView(equations, equationsStepMap);
    }

    private void setUpRecyclerView(List<String> list, HashMap<String, List<String>> equationsStepMap) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        stepsRecyclerView.setLayoutManager(layoutManager);

        StepAdapter solutionAdapter = new StepAdapter(list, StepsListActivity.this, equationsStepMap);
        stepsRecyclerView.setAdapter(solutionAdapter);
    }
}
