package com.navneet.solvemath;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.navneet.solvemath.utils.FileUtils;
import com.navneet.solvemath.utils.GeneratePDF;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    final int MY_CAMERA_PERMISSION_CODE = 1;
    final int MY_EXTERNAL_STORAGE_CODE = 2;
    final int CAMERA_REQUEST = 1;

    private List<String> stepsList;
    private String equation;
    private FirebaseAuth mAuth;

    // This text view will contain the text coming from API
    @BindView(R.id.latexTextView)
    TextView latexTextView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.generatePDFButton)
    Button generatePDFButton;

    @BindView(R.id.signaturePad)
    SignaturePad signaturePad;

    @BindView(R.id.choiceButton)
    Button choiceButton;

    @BindView(R.id.evaluateButton)
    Button evaluateButton;

    @BindView(R.id.drawingDone)
    Button doneDrawingButton;

    @BindView(R.id.saveSolution)
    Button saveSolution;

    @BindView(R.id.showHistory)
    Button showHistory;

    /*
     * Currently there is no API available for all types of calculations.
     * So, we are using different APIs for algebra and different for Simple Calculations
     */

    @OnClick(R.id.choiceButton)
    void choiceButtonPressed() {
        PopupMenu popup = new PopupMenu(this, choiceButton);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_input_choice, popup.getMenu());
        popup.setOnMenuItemClickListener(this::onMenuItemClick);
        popup.show();
    }

    /**
     * This method will be used for simple expressions such as "2+3+5*10+9-8"
     */
    @OnClick(R.id.evaluateButton)
    void evaluateButtonPressed() {
        onEvaluateButtonPressed();
    }

    void algebraExpressionButtonPressed() {

        String mathExpression = latexTextView.getText().toString().trim();
        if (mathExpression.isEmpty()) {
            Toast.makeText(MainActivity.this, "No Expression available to evaluate", Toast.LENGTH_SHORT).show();
            return;
        }

        FetchStepsTask.UploadParams params = new FetchStepsTask.UploadParams(mathExpression);
        FetchStepsTask task = new FetchStepsTask(new FetchStepsTask.ResultListener() {

            @Override
            public void onError(String message) {
                latexTextView.setText(message);
            }

            @Override
            public void onSuccess(List<String> stepDataList) {

                onAlgebraicSuccessfulEvaluation();

                equation = mathExpression;
                stepsList = stepDataList;

                setUpRecyclerView(stepDataList);
            }
        });
        task.execute(params);
    }

    @OnClick(R.id.saveSolution)
    void saveSolutionButtonPressed() {
        if (equation != null && stepsList != null) {
            if (mAuth.getCurrentUser() != null) {
                // Write a message to the database
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                String uID = mAuth.getUid();
                if (uID != null) {
                    DatabaseReference databaseReference = database.getReference(uID).child(equation);
                    if (stepsList != null) {
                        databaseReference.setValue(stepsList);
                    }
                }
            }
        }
    }

    @OnClick(R.id.generatePDFButton)
    void generatePDFButtonPressed() {

        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_EXTERNAL_STORAGE_CODE);
        } else {
            generatePDF();
        }
    }

    @OnClick(R.id.showHistory)
    void showSteps() {
        Intent intent = new Intent(this, StepsListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.drawingDone)
    void drawingDone() {
        drawingDoneButtonClick();
    }

    private void generatePDF() {
        try {
            new GeneratePDF(equation, stepsList).generatePDF(FileUtils.getAppPath(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpRecyclerView(List<String> stepDataList) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        SolutionAdapter solutionAdapter = new SolutionAdapter(stepDataList);
        recyclerView.setAdapter(solutionAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        //getting Firebase auth object
        mAuth = FirebaseAuth.getInstance();

        onLaunchScreen();
    }

    private void uploadImage(Bitmap bitmap) {
        UploadImageTask.UploadParams params = new UploadImageTask.UploadParams(bitmap);
        UploadImageTask task = new UploadImageTask(new UploadImageTask.ResultListener() {
            @Override
            public void onError(String message) {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String latex) {
                latexTextView.setText(latex);
                imageUploadSuccessful();
            }
        });
        task.execute(params);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_EXTERNAL_STORAGE_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                generatePDF();
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(this, "Selected Item: " + item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.camera:
                cameraMenuItemClick();
                return true;
            case R.id.draw:
                drawMenuItemClick();
                return true;
            default:
                return false;
        }
    }

    private void onLaunchScreen() {

        choiceButton.setVisibility(View.VISIBLE);

        latexTextView.setVisibility(View.GONE);
        evaluateButton.setVisibility(View.GONE);
        signaturePad.setVisibility(View.GONE);
        doneDrawingButton.setVisibility(View.GONE);
        saveSolution.setVisibility(View.GONE);
        generatePDFButton.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    private void cameraMenuItemClick() {

        latexTextView.setVisibility(View.GONE);
        evaluateButton.setVisibility(View.GONE);
        signaturePad.setVisibility(View.GONE);
        doneDrawingButton.setVisibility(View.GONE);
        saveSolution.setVisibility(View.GONE);
        generatePDFButton.setVisibility(View.GONE);

        // Open Camera
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        } else {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    private void drawMenuItemClick() {

        signaturePad.setVisibility(View.VISIBLE);
        doneDrawingButton.setVisibility(View.VISIBLE);

        latexTextView.setVisibility(View.GONE);
        evaluateButton.setVisibility(View.GONE);
        saveSolution.setVisibility(View.GONE);
        generatePDFButton.setVisibility(View.GONE);
    }

    private void drawingDoneButtonClick() {

        Bitmap bitmap = signaturePad.getSignatureBitmap();
        signaturePad.setVisibility(View.GONE);
        uploadImage(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data.getExtras() != null) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                uploadImage(photo);
            }
        }
    }

    private void imageUploadSuccessful() {

        latexTextView.setVisibility(View.VISIBLE);
        evaluateButton.setVisibility(View.VISIBLE);

        signaturePad.setVisibility(View.GONE);
        doneDrawingButton.setVisibility(View.GONE);
        saveSolution.setVisibility(View.GONE);
        generatePDFButton.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    private void onEvaluateButtonPressed() {

        String mathExpression = latexTextView.getText().toString().trim();
        if (mathExpression.isEmpty()) {
            Toast.makeText(MainActivity.this, "No Expression available to evaluate", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            Double result = new DoubleEvaluator().evaluate(mathExpression);
            latexTextView.setVisibility(View.VISIBLE);
            latexTextView.setText(String.format("%s", result));

        } catch (Exception e) {
            e.printStackTrace();
            algebraExpressionButtonPressed();
        }
    }

    private void onAlgebraicSuccessfulEvaluation() {

        latexTextView.setVisibility(View.VISIBLE);
        saveSolution.setVisibility(View.VISIBLE);
        generatePDFButton.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);

        evaluateButton.setVisibility(View.GONE);
        signaturePad.setVisibility(View.GONE);
        doneDrawingButton.setVisibility(View.GONE);
    }
}